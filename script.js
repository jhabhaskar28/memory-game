let startButton = document.querySelector('.gameStartButton');
startButton.addEventListener('click',startGame);

let viewScoresButton = document.querySelector('.viewScoresButton');
viewScoresButton.addEventListener('click',viewScores);

let resetButton = document.querySelector('.resetButton');
resetButton.addEventListener('click',resetHighScore);

let backButton = document.querySelector('.backButton');
backButton.addEventListener('click',displayHomePage);

let easyLevel = document.querySelector('.easyLevelButton');
easyLevel.addEventListener('click', fillImagesForEasy);

let mediumLevel = document.querySelector('.mediumLevelButton');
mediumLevel.addEventListener('click',fillImagesForMedium);

let hardLevel = document.querySelector('.hardLevelButton');
hardLevel.addEventListener('click',fillImagesForHard);

let game = document.querySelector('.gameWithScore');
game.style.display = "none";  

let scoreBoard = document.querySelector('.scoreBoard');
scoreBoard.style.display = "none";

let gameStart = document.querySelector('.gameStart');
let resetMessage = document.querySelector('.resetMessage');

let level = document.querySelector('.gameLevel');
level.style.display = "none";

let levelType = "";
let images = [];

function startGame(){
  gameStart.style.display = "none";

  level.style.display = "flex";
}

function resetHighScore(){
  resetMessage.style.display = "flex";
  localStorage.clear();
  resetScore();
}

function displayHomePage(){
  scoreBoard.style.display = "none";
  resetMessage.style.display = "none";
  gameStart.style.display = "flex";
}

let easyScore = document.createElement('h1');
let mediumScore = document.createElement('h1');
let hardScore = document.createElement('h1');

function viewScores(){

  let resetButton = document.querySelector('.resetButton');

  scoreBoard.insertBefore(easyScore,resetButton);
  scoreBoard.insertBefore(mediumScore,resetButton);
  scoreBoard.insertBefore(hardScore,resetButton);

  resetScore();

  gameStart.style.display = "none";
  scoreBoard.style.display = "flex";
}

function resetScore(){
  if(localStorage.getItem("Easy Score") === null){
    easyScore.textContent = "EASY SCORE: 0";
  } else {
    easyScore.textContent = `EASY SCORE: ${localStorage.getItem("Easy Score")}`;
  }

  if(localStorage.getItem("Medium Score") === null){
    mediumScore.textContent = "MEDIUM SCORE: 0";
  } else {
    mediumScore.textContent = `MEDIUM SCORE: ${localStorage.getItem("Medium Score")}`;
  }

  if(localStorage.getItem("Hard Score") === null){
    hardScore.textContent = "HARD SCORE: 0";
  } else {
    hardScore.textContent = `HARD SCORE: ${localStorage.getItem("Hard Score")}`;
  }
}

function fillImagesForEasy(){
  images = [
  "1.gif",
  "2.gif",
  "3.gif",
  "4.gif",
  "5.gif",
  "6.gif",
  "1.gif",
  "2.gif",
  "3.gif",
  "4.gif",
  "5.gif",
  "6.gif"
  ];

  levelType = "Easy";
  createLevel();
}


function fillImagesForMedium(){
  images = [
  "1.gif",
  "2.gif",
  "3.gif",
  "4.gif",
  "5.gif",
  "6.gif",
  "7.gif",
  "8.gif",
  "9.gif",
  "1.gif",
  "2.gif",
  "3.gif",
  "4.gif",
  "5.gif",
  "6.gif",
  "7.gif",
  "8.gif",
  "9.gif"
  ];

  levelType = "Medium";
  createLevel();
}

function fillImagesForHard(){
  images = [
  "1.gif",
  "2.gif",
  "3.gif",
  "4.gif",
  "5.gif",
  "6.gif",
  "7.gif",
  "8.gif",
  "9.gif",
  "10.gif",
  "11.gif",
  "12.gif",
  "1.gif",
  "2.gif",
  "3.gif",
  "4.gif",
  "5.gif",
  "6.gif",
  "7.gif",
  "8.gif",
  "9.gif",
  "10.gif",
  "11.gif",
  "12.gif"
  ];

  levelType = "Hard";
  createLevel();
}


function createLevel(){

  let shuffledImages = shuffle(images);
  createDivsForImages(shuffledImages,levelType);

  let level = document.querySelector('.gameLevel');
  level.style.display = "none";

  createScoreBoard(levelType);
  game.style.display = "flex"; 
}

function createScoreBoard(){

  let finalScoreDiv = document.createElement('div');
  finalScoreDiv.className = "finalScoreContainer";

  let highestScoreDiv = document.createElement('div');
  highestScoreDiv.className = "highestScoreContainer";

  let highestScoreHeading = document.createElement('h1');
  highestScoreHeading.textContent = "HIGH SCORE:";

  let highestScore = document.createElement('h1');
  highestScore.className = "highestScore";

  highestScoreDiv.appendChild(highestScoreHeading);
  highestScoreDiv.appendChild(highestScore);

  let guessDiv = document.createElement('div');
  guessDiv.className = "guessContainer";

  let guessHeading = document.createElement('h1');
  guessHeading.textContent = "GUESSES:";

  let remainingGuess = document.createElement('h1');
  remainingGuess.className = "remainingGuess";

  if(levelType === "Easy"){
    remainingGuess.textContent = "30";
  } else if(levelType === "Medium"){
    remainingGuess.textContent = "40";
  } else {
    remainingGuess.textContent = "60";
  }

  guessDiv.appendChild(guessHeading);
  guessDiv.appendChild(remainingGuess);

  let scoreDiv = document.createElement('div');
  scoreDiv.className = "scoreContainer";

  let scoreHeading = document.createElement('h1');
  scoreHeading.textContent = "SCORE:";

  let score = document.createElement('h1');
  score.className = "gameScore";
  score.textContent = "0";

  scoreDiv.appendChild(scoreHeading);
  scoreDiv.appendChild(score);

  finalScoreDiv.appendChild(highestScoreDiv);
  finalScoreDiv.appendChild(guessDiv);
  finalScoreDiv.appendChild(scoreDiv);

  const gameContainer = document.getElementById("game");

  let gameWithScore = document.querySelector('.gameWithScore');
  gameWithScore.insertBefore(finalScoreDiv,gameContainer);


  if(localStorage.getItem(`${levelType} Score`) === null){
    highestScore.textContent = "0";
  } else {
    let highScore = localStorage.getItem(`${levelType} Score`);
    highestScore.textContent = highScore; 
  }

}

// here is a helper function to shuffle an array
// it returns the same array with values shuffled
// it is based on an algorithm called Fisher Yates if you want ot research more
function shuffle(array) {
  let counter = array.length;

  // While there are elements in the array
  while (counter > 0) {
    // Pick a random index
    let index = Math.floor(Math.random() * counter);

    // Decrease counter by 1
    counter--;

    // And swap the last element with it
    let temp = array[counter];
    array[counter] = array[index];
    array[index] = temp;
  }

  return array;
}

// this function loops over the array of colors
// it creates a new div and gives it a class with the value of the color
// it also adds an event listener for a click for each card
function createDivsForImages(imageArray) {
  let arrayCounter = 0;

  for (let color of imageArray) {
    arrayCounter += 1;
    // create a new div
    const newDiv = document.createElement("div");

    // give it a class attribute for the value we are looping over
    newDiv.classList.add(color);

    // call a function handleCardClick when a div is clicked on
    newDiv.addEventListener("click", handleCardClick);

    const gameContainer = document.getElementById("game");

    // append the div to the element with an id of game
    gameContainer.append(newDiv);
  }
}

let previousEvent = null;
let clickCount = 0;
let matchCounter = 0; 
let completedFlag = 0;



// TODO: Implement this function!
function handleCardClick(event) {

  let remainingGuess = document.querySelector('.remainingGuess');
  let remainingGuessCount = parseInt(remainingGuess.textContent);
  let gameScore = document.querySelector(".gameScore");

  if ( (event.target.style.backgroundImage === '') && clickCount < 2) {

    event.target.style.backgroundImage = `url(./gifs/${event.target.className})`;
    event.target.style.backgroundSize = "cover";
    event.target.style.backgroundPosition = "center";
    event.target.style.backgroundRepeat = "no-repeat";
    clickCount += 1;

    if (clickCount === 1) {
      previousEvent = event;

    } else {
      if (event.target.className !== previousEvent.target.className) {

        setTimeout(() => {
          event.target.style.backgroundImage = '';
          previousEvent.target.style.backgroundImage = '';

          event.target.style.backgroundPosition = "center";
          event.target.style.backgroundSize = "contain";
          event.target.style.backgroundRepeat = "no-repeat";
          previousEvent.target.style.backgroundPosition = "center";
          previousEvent.target.style.backgroundSize = "contain";
          previousEvent.target.style.backgroundRepeat = "no-repeat";

          clickCount %= 2;
        }, 1000);

      } else {
        clickCount %= 2;
        matchCounter += 1;
        let score = parseInt(gameScore.textContent);

        if(levelType === 'Easy'){
          score += 60;
        } else if(levelType === 'Medium'){
          score += 40;
        } else {
          score += 30;
        }
        
        gameScore.textContent = `${score}`;
        localStorage.setItem(`${levelType} Score`,`${score}`);

        let highestScore = document.querySelector('.highestScore');

        if(score > parseInt(highestScore.textContent)){
          highestScore.textContent = `${score}`;
        }
      }
    }

    if(remainingGuessCount > 0){
      remainingGuessCount -= 1;
    }
    
    remainingGuess.textContent = remainingGuessCount;
  }

  
  
  if ((matchCounter === (images.length/2) || remainingGuessCount === 0) && completedFlag === 0) {

    completedFlag = 1;
    setTimeout(()=> {

      let completed = document.createElement("div");
      completed.className = "completed";

      let game = document.querySelector(".gameWithScore");
      game.style.display = "none";

      let successMessage = document.createElement("h1");
      successMessage.className = "successMessage";

      let scoreMessage = document.createElement("h1");
      scoreMessage.className = "scoreMessage";
      scoreMessage.textContent = `SCORE: ${gameScore.textContent}`;
      
      if(matchCounter === (images.length/2)){
        successMessage.textContent = "Success! You have matched all the cards.";
      } else {
        successMessage.textContent = "Sorry! You have exhausted your guesses.";
      }

      completed.appendChild(successMessage);
      completed.appendChild(scoreMessage);

      let pageReload = document.createElement("a");
      pageReload.className = "pageReload";
      pageReload.setAttribute("href", "./index.html");

      let playAgain = document.createElement("input");
      playAgain.className = "playAgainButton";
      playAgain.setAttribute("type", "button");
      playAgain.setAttribute("value", "PLAY AGAIN");
      playAgain.addEventListener('click',restartGame);

      let home = document.createElement("input");
      home.className = "home";
      home.setAttribute("type", "button");
      home.setAttribute("value", "HOME");

      pageReload.appendChild(home);

      completed.appendChild(pageReload);
      completed.appendChild(playAgain);


      let gameWithScore = document.querySelector('.gameWithScore');
      let body = document.querySelector('body');
      body.insertBefore(completed,gameWithScore);
      completed.style.display = "flex";
    },1000);
  }
}

function restartGame(){

  let presentGame = document.querySelector('#game');
  presentGame.remove();

  let newGame = document.createElement('div');
  newGame.id = "game";

  let finalScoreContainer = document.querySelector('.finalScoreContainer');
  finalScoreContainer.remove();

  let completed = document.querySelector('.completed');
  completed.remove();
  completedFlag = 0;

  previousEvent = null;
  clickCount = 0;
  matchCounter = 0; 
  completedFlag = 0;

  game.appendChild(newGame);

  if(levelType === "Easy"){
    fillImagesForEasy();
  } else if(levelType === "Medium"){
    fillImagesForMedium();
  } else {
    fillImagesForHard();
  }
}

